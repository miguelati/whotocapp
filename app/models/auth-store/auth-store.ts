import { Instance, SnapshotOut, types, addDisposer, flow } from "mobx-state-tree"
import { Platform } from "react-native"
import auth from "@react-native-firebase/auth"
import { UserModel } from "../user/user"
import { ProfileModel } from "../profile/profile"
import { DiscoverModel } from "../discover/discover"
import firestore from "@react-native-firebase/firestore"
import storage from "@react-native-firebase/storage"

/**
 * Model description here for TypeScript hints.
 */
export const AuthStoreModel = types
  .model("AuthStore")
  .props({
    user: types.optional(types.union(UserModel, types.literal(null)), null),
    profile: types.optional(types.union(ProfileModel, types.literal(null)), null),
    isUserPending: types.boolean,
    isNewUser: types.optional(types.union(types.boolean, types.literal(true)), true),
    discover: types.optional(types.union(DiscoverModel, types.literal(null)), null),
  })
  .volatile((self) => ({
    ownProfile: null,
  }))
  .views((self) => ({
    get isAuthenticated(): boolean {
      return self.user !== null
    },
    get hasProfile(): boolean {
      return self.profile !== null
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setProfile: (profile: {}) => {
      self.profile = profile
    },
  }))
  .actions((self) => ({
    setUser: flow(function* (user: Instance<typeof UserModel>) {
      self.isUserPending = false
      if (user) {
        const profileDoc = yield firestore().doc(`profiles/${user.uid}`).get()

        if (profileDoc.exists) {
          self.isNewUser = false

          self.setProfile({
            uid: user.uid,
            firstName: profileDoc.get("firstName"),
            lastName: profileDoc.get("lastName"),
            bio: profileDoc.get("bio"),
            bornDate: profileDoc.get("bornDate").toDate(),
            blockedUsers: profileDoc.get("blockedUsers"),
            lastUpdate: profileDoc.get("lastUpdate").toDate(),
            os: profileDoc.get("os"),
            picture: profileDoc.get("picture"),
            picturePath: profileDoc.get("picturePath"),
            profession: profileDoc.get("profession"),
            collage: profileDoc.get("collage"),
            sex: profileDoc.get("sex"),
            token: profileDoc.get("token"),
            visible: profileDoc.get("visible"),
          })
        }

        const snapshot = (snapshot) => {
          if (snapshot === null && self.ownProfile !== null) {
            self.ownProfile()
          } else if (snapshot.exists) {
            self.setProfile({
              uid: user.uid,
              firstName: snapshot.get("firstName"),
              lastName: snapshot.get("lastName"),
              bio: snapshot.get("bio"),
              bornDate: snapshot.get("bornDate").toDate(),
              blockedUsers: snapshot.get("blockedUsers"),
              lastUpdate: snapshot.get("lastUpdate").toDate(),
              os: snapshot.get("os"),
              picture: snapshot.get("picture"),
              picturePath: snapshot.get("picturePath"),
              profession: snapshot.get("profession"),
              collage: snapshot.get("collage"),
              sex: snapshot.get("sex"),
              token: snapshot.get("token"),
              visible: snapshot.get("visible"),
            })
          }
        }

        self.ownProfile = firestore().doc(`profiles/${user.uid}`).onSnapshot(snapshot)

        self.user = {
          uid: user.uid,
          phoneNumber: user.phoneNumber,
          isAnonymous: user.isAnonymous,
        }
      } else {
        self.user = null
      }
    }),
    createProfile: flow(function* (profile: Instance<typeof ProfileModel>) {
      self.isUserPending = true
      yield firestore()
        .doc(`profiles/${self.user.uid}`)
        .set(
          {
            firstName: profile.firstName,
            lastName: profile.lastName,
            bornDate: profile.bornDate,
            sex: profile.sex,
            profession: profile.profession,
            collage: profile.collage,

            lastUpdate: new Date(),
            os: Platform.OS === "ios" ? "ios" : "android",
          },
          { merge: true },
        )
      if (profile) {
        self.profile = profile
        self.isUserPending = false
      } else {
        self.profile = null
        self.isUserPending = false
      }
    }),
    uploadProfilePicture: flow(function* (imageUri: string) {
      const picturePath = `userPicture/${self.user.uid}/profile.jpg`
      const store = storage().ref(picturePath)
      yield new Promise((resolve, reject) => {
        store
          .putFile(imageUri)
          .then((test) => {
            resolve(test)
          })
          .catch((e) => {
            reject(e)
          })
      })
      const pictureUrl = yield store.getDownloadURL()
      return { pictureUrl, picturePath }
    }),
  }))
  .actions((self) => ({
    subscribe: (
      callback?: (user: typeof UserModel | null, profile: typeof ProfileModel | null) => void,
    ) => {
      const onAuthStateChanged = async (user) => {
        if (user) {
          await self.setUser(user)
          if (callback) callback(self.user, self.profile)
        } else {
          // @ts-ignore
          await self.setUser(null, null)
          if (callback) callback(null, null)
        }
      }
      const disposer = auth().onAuthStateChanged(onAuthStateChanged)
      addDisposer(self, disposer)
    },
    signInWithPhone(phoneNumber: string) {
      return auth().signInWithPhoneNumber(phoneNumber)
    },
    resendCodeToPhone(phoneNumber: string) {
      console.log("resend")
      return auth().signInWithPhoneNumber(phoneNumber, true)
    },
    signOut: flow(function* () {
      yield auth().signOut()
      self.user = null
      self.profile = null
    }),
    addProfilePicture: flow(function* (imageUri: string) {
      self.isUserPending = true
      const picture = yield self.uploadProfilePicture(imageUri)
      yield firestore().doc(`profiles/${self.user.uid}`).set(
        {
          picture: picture.pictureUrl,
          picturePath: picture.picturePath,
          lastUpdate: new Date(),
        },
        { merge: true },
      )
      self.profile.picture = picture.pictureUrl
      self.profile.picturePath = picture.picturePath
      self.isUserPending = false
    }),
    addBio: flow(function* (bio: string) {
      self.isUserPending = true
      yield firestore().doc(`profiles/${self.user.uid}`).set(
        {
          bio,
        },
        { merge: true },
      )
      self.profile.bio = bio
      self.isUserPending = false
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type AuthStoreType = Instance<typeof AuthStoreModel>
export interface AuthStore extends AuthStoreType {}
type AuthStoreSnapshotType = SnapshotOut<typeof AuthStoreModel>
export interface AuthStoreSnapshot extends AuthStoreSnapshotType {}
