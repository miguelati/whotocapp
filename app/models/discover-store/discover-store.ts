import { SnapshotOrInstance, Instance, SnapshotOut, types, flow, getParent } from "mobx-state-tree"
import { DiscoverModel } from "../discover/discover"
import { RootStore } from "../root-store/root-store"
import firestore from "@react-native-firebase/firestore"
import geohash from "ngeohash"

/**
 * Model description here for TypeScript hints.
 */
export const DiscoverStoreModel = types
  .model("DiscoverStore")
  .props({
    discovers: types.array(DiscoverModel),
    actual: types.maybeNull(DiscoverModel),
  })
  .volatile((self) => ({
    radar: null,
  }))
  .views((self) => ({
    isInRadar(discover: SnapshotOrInstance<typeof DiscoverModel>) {
      console.log(self.discovers.entries)
      console.log(discover)
      return true
    },
    inRadar: () => {
      const root: RootStore = getParent(self)
      const inRadar = self.discovers.filter(
        (item) =>
          root.authStore.profile !== null && item.profile.uid !== root.authStore.profile.uid,
      )
      return inRadar
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    load: (users: [SnapshotOrInstance<typeof DiscoverModel>]) => {
      self.discovers.clear()
      users.forEach((item: SnapshotOrInstance<typeof DiscoverModel>) => {
        self.discovers.push(item)
      })
    },
  }))
  .actions((self) => ({
    getInRadar: (latitude: number, longitude: number) => {
      if (self.radar !== null) {
        self.radar()
      }
      const onSnapshot = (snap) => {
        if (snap === null && self.radar !== null) {
          self.radar()
        } else {
          const data = snap.docs.map((doc) => {
            const { latitude, longitude } = doc.get("position")
            const profile = doc.get("profile")
            return DiscoverModel.create({
              latitude: latitude,
              longitude: longitude,
              profile: {
                uid: profile.uid,
                firstName: profile.firstName,
                lastName: profile.lastName,
                bio: profile.bio,
                bornDate: profile.bornDate.toDate(),
                lastUpdate: profile.lastUpdate.toDate(),
                os: profile.os,
                picture: profile.picture,
                picturePath: profile.picturePath,
                profession: profile.profession,
                collage: profile.collage,
                sex: profile.sex,
                token: profile.token,
                visible: profile.visible,
              },
            })
          })
          self.load(data)
        }
      }
      const h = geohash.encode_int(latitude, longitude, 10)

      self.radar = firestore()
        .collection(`discover`)
        .where("hash", ">=", geohash.neighbor_int(h, [-1, -1], 10))
        .where("hash", "<=", geohash.neighbor_int(h, [1, 1], 10))
        .onSnapshot(onSnapshot)
    },
  }))
  .actions((self) => ({
    add: flow(function* (latitude: number, longitude: number) {
      const root: RootStore = getParent(self)

      yield firestore()
        .doc(`discover/${root.authStore.profile.uid}`)
        .set(
          {
            profile: { ...root.authStore.profile },
            position: new firestore.GeoPoint(latitude, longitude),
            hash: geohash.encode_int(latitude, longitude, 10),
          },
          { merge: true },
        )
      self.getInRadar(latitude, longitude)
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type DiscoverStoreType = Instance<typeof DiscoverStoreModel>
export interface DiscoverStore extends DiscoverStoreType {}
type DiscoverStoreSnapshotType = SnapshotOut<typeof DiscoverStoreModel>
export interface DiscoverStoreSnapshot extends DiscoverStoreSnapshotType {}
