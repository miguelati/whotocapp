import { DiscoverStoreModel } from "../discover-store/discover-store"
import { AuthStoreModel } from "../auth-store/auth-store"
import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * A RootStore model.
 */
// prettier-ignore
export const RootStoreModel = types.model("RootStore").props({
  discoverStore: types.optional(DiscoverStoreModel, {
    discovers: []
  }),
  authStore: types.optional(AuthStoreModel, {
    user: null,
    profile: null,
    isUserPending: true,
  }),
})

/**
 * The RootStore instance.
 */
export interface RootStore extends Instance<typeof RootStoreModel> {}

/**
 * The data of a RootStore.
 */
export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> {}
