import { Instance, SnapshotOut, types } from "mobx-state-tree"
import moment from "moment"

export const Sex = {
  MALE: "male",
  FEMALE: "female",
}

export const MobileSystem = {
  IOS: "ios",
  ANDROID: "android",
}

/**
 * Model description here for TypeScript hints.
 */
export const ProfileModel = types
  .model("Profile")
  .props({
    uid: types.optional(types.maybeNull(types.identifier), null),
    firstName: types.string,
    lastName: types.string,
    bio: types.optional(types.maybeNull(types.string), null),
    bornDate: types.Date,
    blockedUsers: types.optional(types.array(types.string), []),
    lastUpdate: types.optional(types.Date, new Date()),
    os: types.enumeration([MobileSystem.IOS, MobileSystem.ANDROID]),
    picture: types.optional(types.maybeNull(types.string), null),
    picturePath: types.optional(types.maybeNull(types.string), null),
    profession: types.string,
    collage: types.string,
    sex: types.enumeration([Sex.MALE, Sex.FEMALE]),
    token: types.optional(types.maybeNull(types.string), ""),
    visible: types.optional(types.boolean, true),
  })
  .views((self) => ({
    getAge: () => {
      return moment().diff(moment(self.bornDate), "year")
    },
    getFullName: () => {
      return `${self.firstName} ${self.lastName}`
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .views((self) => ({
    getNameWithAge: (fullName = true) => {
      return fullName
        ? `${self.getFullName()} • ${self.getAge()}`
        : `${self.firstName} • ${self.getAge()}`
    },
  }))
  .actions((self) => ({
    getJson: () => {
      return {
        uid: self.uid.toString(),
        firstName: self.firstName.toString(),
        lastName: self.lastName.toString(),
        bio: self.bio.toString(),
        bornDate: self.bornDate,
        blockedUsers: self.blockedUsers,
        lastUpdate: self.lastUpdate,
        os: self.os.toString(),
        picture: self.picture.toString(),
        picturePath: self.picturePath.toString(),
        profession: self.profession.toString(),
        collage: self.collage.toString(),
        sex: self.collage.toString(),
        token: self.token.toString(),
        visible: self.visible,
      }
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type ProfileType = Instance<typeof ProfileModel>
export interface Profile extends ProfileType {}
type ProfileSnapshotType = SnapshotOut<typeof ProfileModel>
export interface ProfileSnapshot extends ProfileSnapshotType {}
