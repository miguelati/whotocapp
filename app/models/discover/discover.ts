import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { ProfileModel } from "../profile/profile"

/**
 * Model description here for TypeScript hints.
 */
export const DiscoverModel = types
  .model("Discover")
  .props({
    profile: types.optional(types.union(ProfileModel, types.literal(null)), null),
    latitude: types.optional(types.maybeNull(types.number), null),
    longitude: types.optional(types.maybeNull(types.number), null),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type DiscoverType = Instance<typeof DiscoverModel>
export interface Discover extends DiscoverType {}
type DiscoverSnapshotType = SnapshotOut<typeof DiscoverModel>
export interface DiscoverSnapshot extends DiscoverSnapshotType {}
