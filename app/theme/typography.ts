import { Platform } from "react-native"

/**
 * You can find a list of available fonts on both iOS and Android here:
 * https://github.com/react-native-training/react-native-fonts
 *
 * If you're interested in adding a custom font to your project,
 * check out the readme file in ./assets/fonts/ then come back here
 * and enter your new font name. Remember the Android font name
 * is probably different than iOS.
 * More on that here:
 * https://github.com/lendup/react-native-cross-platform-text
 *
 * The various styles of fonts are defined in the <Text /> component.
 */
export const typography = {
  /**
   * The primary font.  Used in most places.
   */
  regular: Platform.select({ ios: "Dosis-Regular", android: "Dosis-Regular" }),

  /**
   * An alternate font used for perhaps titles and stuff.
   */
  medium: Platform.select({ ios: "Dosis-Medium", android: "sans-serif" }),

  /**
   * Lets get fancy with a monospace font!
   */
  light: Platform.select({ ios: "Dosis-Light", android: "Dosis-Light" }),
  extraLight: Platform.select({ ios: "Dosis-ExtraLight", android: "Dosis-ExtraLight" }),
  semiBold: Platform.select({ ios: "Dosis-SemiBold", android: "Dosis-SemiBold" }),
  bold: Platform.select({ ios: "Dosis-Bold", android: "Dosis-Bold" }),
  extraBold: Platform.select({ ios: "Dosis-ExtraBold", android: "Dosis-ExtraBold" }),
  SFCompact: Platform.select({
    ios: "SF Compact Rounded",
    android: "SF-Compact-Rounded",
  }),
  SFPro: Platform.select({
    ios: "SF-Pro-Rounded",
    android: "SF-Pro-Rounded",
  }),
}
