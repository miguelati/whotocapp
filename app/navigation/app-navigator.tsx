/**
 * This is the navigator you will modify to display the logged-in screens of your app.
 * You can use RootNavigator to also display an auth flow or other user flows.
 *
 * You'll likely spend most of your time in this file.
 */
import React from "react"

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { AppProfileNavigator } from "./app-profile-navigator"
import { AppDiscoverNavigator } from "./app-discover-navigator"
import { AppMessagesNavigator } from "./app-messages-navigator"
import { useStores } from "../models"
import { observer } from "mobx-react-lite"
import { TabBar } from "../components"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * If no params are allowed, pass through `undefined`. Generally speaking, we
 * recommend using your MobX-State-Tree store(s) to keep application state
 * rather than passing state through navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */
export type AppParamList = {
  appProfile: undefined
  appDiscover: undefined
  appMessages: undefined
}

// Documentation: https://github.com/software-mansion/react-native-screens/tree/master/native-stack
const Tab = createBottomTabNavigator<AppParamList>()

export const AppNavigator = observer(function AppNavigator() {
  const rootStore = useStores()

  return (
    <Tab.Navigator initialRouteName={rootStore.authStore.isNewUser ? "appMessages" : "appDiscover"} tabBar={(props) => <TabBar {...props} />}>
      <Tab.Screen name="appProfile" component={AppProfileNavigator} />
      <Tab.Screen name="appDiscover" component={AppDiscoverNavigator} />
      <Tab.Screen name="appMessages" component={AppMessagesNavigator} />
    </Tab.Navigator>
  )
})

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["appProfile"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
