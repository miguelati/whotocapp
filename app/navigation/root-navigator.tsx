/**
 * The root navigator is used to switch between major navigation flows of your app.
 * Generally speaking, it will contain an auth flow (registration, login, forgot password)
 * and a "main" flow (which is contained in your PrimaryNavigator) which the user
 * will use once logged in.
 */
import React from "react"
import { NavigationContainer, NavigationContainerRef } from "@react-navigation/native"
import { createStackNavigator, TransitionPresets } from "@react-navigation/stack"
import { AuthNavigator } from "./auth-navigator"
import { AppNavigator } from "./app-navigator"
import { WizNavigator } from "./wiz-navigator"
import { useStores } from "../models"
import { SplashScreen } from "../screens"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * We recommend using MobX-State-Tree store(s) to handle state rather than navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */
export type RootParamList = {
  authNavigator: undefined
  appNavigator: undefined
  wizNavigator: undefined
  splashScreen: undefined
}

const Stack = createStackNavigator<RootParamList>()

const RootStack = () => {
  const [user, setUser] = React.useState(null)
  const [profile, setProfile] = React.useState(null)
  const [loading, setLoading] = React.useState(true)

  const rootStore = useStores()

  React.useEffect(() => {
    rootStore.authStore.subscribe((user, profile) => {
      setUser(user)
      setProfile(profile)
      setLoading(false)
    })
  }, [])

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        ...TransitionPresets.ModalTransition,
      }}
    >
      {user === null && profile === null && loading && (
        <Stack.Screen name="splashScreen" component={SplashScreen} />
      )}
      {user === null && !loading && (
        <Stack.Screen
          name="authNavigator"
          component={AuthNavigator}
          options={{
            headerShown: false,
          }}
        />
      )}
      {user !== null && (
        <>
          {profile === null && <Stack.Screen name="wizNavigator" component={WizNavigator} />}
          <Stack.Screen name="appNavigator" component={AppNavigator} />
        </>
      )}
    </Stack.Navigator>
  )
}

export const RootNavigator = React.forwardRef<
  NavigationContainerRef,
  Partial<React.ComponentProps<typeof NavigationContainer>>
>((props, ref) => {
  return (
    <NavigationContainer {...props} ref={ref}>
      <RootStack />
    </NavigationContainer>
  )
})

RootNavigator.displayName = "RootNavigator"
