import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle } from "react-native"
import { Container, Grid, Row, Col } from "native-base"
import OTPInputView from "@twotalltotems/react-native-otp-input"
import { Screen, Text, NavigationBar, Hud } from "../../components"
import { useRoute, useFocusEffect } from "@react-navigation/native"
import { useStores } from "../../models"
import { color, typography } from "../../theme"
import { translate } from "../../i18n"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const TITLE_CONTAINER: ViewStyle = {
  height: 50,
  alignItems: "flex-end",
}

const DESCRIPTION_CONTAINER: ViewStyle = {
  height: 45,
  marginTop: 10,
  alignItems: "flex-start",
}

const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 24,
  color: color.primary,
  marginLeft: 30,
}

const DESCRIPTION: TextStyle = {
  fontFamily: typography.medium,
  fontSize: 14,
  color: color.palette.navy,
  marginLeft: 30,
  marginRight: 30,
}

const OTP_CONTAINER: ViewStyle = {
  width: "90%",
  height: "100%",
  alignSelf: "center",
}

const OPT_INPUT_FIELD: TextStyle = {
  fontFamily: typography.SFCompact,
  fontSize: 48,
  width: 60,
  height: 135,
  borderWidth: 0,
  color: color.palette.orange,
}

const OPT_INPUT_HIGHLIGHT: TextStyle = {
  color: color.palette.navy,
  // borderColorline: color.palette.navy,
}

const TEXT_BOTTOM: TextStyle = {
  fontFamily: typography.semiBold,
  fontSize: 12,
  color: color.palette.navy,
  textAlign: "center",
}

const TEXT_BOTTOM_BOLD: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 12,
  color: color.palette.navy,
  textAlign: "center",
}

const TEXT_ERROR: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.palette.red,
  textAlign: "center",
}

type RoutesParams = {
  params: { phoneNumber: string }
  key: string
  name: string
}

export const AuthVerifyScreen = observer(function AuthVerifyScreen() {
  const store = useStores()
  const route = useRoute<RoutesParams>()
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  const [checker, setChecker] = React.useState(null)
  const [error, setError] = React.useState(null)
  const [hudVisible, setHudVisible] = React.useState(false)

  useFocusEffect(
    React.useCallback(() => {
      const { phoneNumber } = route.params
      if (checker === null) {
        store.authStore
          .signInWithPhone(phoneNumber)
          .then((checker) => setChecker(checker))
          .catch((error) => setError(translate(`authVerifyScreen.validation.${error.code}`)))
      }
    }, []),
  )

  const onCodeComplete = async (code: string) => {
    try {
      setHudVisible(true)
      await checker.confirm(code)
      setHudVisible(false)
    } catch (error) {
      console.log(error.code)
    }
  }

  // Pull in navigation via hook

  return (
    <Screen style={ROOT} preset="scroll">
      <Container>
        <NavigationBar canBack />
        <Grid>
          <Row style={TITLE_CONTAINER}>
            <Col>
              <Text style={TITLE} tx="authVerifyScreen.title" />
            </Col>
          </Row>
          <Row style={DESCRIPTION_CONTAINER}>
            <Col>
              <Text style={DESCRIPTION} tx="authVerifyScreen.description" />
            </Col>
          </Row>
          <Row>
            <Col>
              <OTPInputView
                style={OTP_CONTAINER}
                placeholderCharacter="0"
                placeholderTextColor={color.palette.grey}
                pinCount={6}
                autoFocusOnLoad
                codeInputFieldStyle={OPT_INPUT_FIELD}
                codeInputHighlightStyle={OPT_INPUT_HIGHLIGHT}
                onCodeFilled={onCodeComplete}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              {error && <Text style={TEXT_ERROR} text={error} />}
              <Text style={TEXT_BOTTOM} tx="authVerifyScreen.bottonText" />
              <Text style={TEXT_BOTTOM_BOLD} tx="authVerifyScreen.bottonTextBold" />
            </Col>
          </Row>
        </Grid>
      </Container>
      <Hud isVisible={hudVisible} loading={true} />
    </Screen>
  )
})
