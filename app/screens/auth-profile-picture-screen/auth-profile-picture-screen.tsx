import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle } from "react-native"
import { Container, Grid, Row, Col } from "native-base"
import { Screen, Text, ButtonGradient, UploadPicture, Hud } from "../../components"
import { useNavigation } from "@react-navigation/native"
import { useStores } from "../../models"
import { color, typography } from "../../theme"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 24,
  color: color.palette.orange,
}

const SUBTITLE: TextStyle = {
  fontFamily: typography.medium,
  fontSize: 14,
  color: color.palette.navy,
  marginRight: 110,
}

const TITLE_CONTAINER: ViewStyle = {
  marginLeft: 30,
}

const PICTURE_CONTAINER: ViewStyle = {
  alignItems: "center",
  justifyContent: "center",
}

export const AuthProfilePictureScreen = observer(function AuthProfilePictureScreen() {
  const [image, setImage] = React.useState(null)
  const [loading, setLoading] = React.useState(false)
  const rootStore = useStores()
  const navigation = useNavigation()

  const onContinueTap = async () => {
    setLoading(true)
    await rootStore.authStore.addProfilePicture(image.path)
    setLoading(false)
    navigation.navigate("authFinish")
  }

  const onPictureChanged = async (image) => {
    setImage(image)
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <Container>
        <Grid>
          <Row />
          <Row>
            <Col style={TITLE_CONTAINER}>
              <Text style={TITLE} tx="authProfilePictureScreen.title" />
              <Text style={SUBTITLE} tx="authProfilePictureScreen.subtitle" />
            </Col>
          </Row>
          <Row size={3}>
            <Col style={PICTURE_CONTAINER}>
              <UploadPicture onPictureChanged={onPictureChanged} />
            </Col>
          </Row>
          <Row>
            <Col>
              <ButtonGradient
                disabled={image === false}
                onPress={onContinueTap}
                tx="authProfilePictureScreen.button"
              />
            </Col>
          </Row>
          <Row />
          <Hud isVisible={loading} loading={true} />
        </Grid>
      </Container>
    </Screen>
  )
})
