import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, Image, ImageStyle, TextStyle, Dimensions } from "react-native"
import { Container, Grid, Row, Col } from "native-base"
import { Screen, NavigationBar, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
import { useStores } from "../../models"
import { color, typography } from "../../theme"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const PROFILE_IMAGE: ImageStyle = {
  width: "100%",
  height: "100%",
}

const USER_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 28,
  color: color.palette.navy,
  textAlign: "center",
  marginBottom: 5,
}

const PROFESSION_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 18,
  color: color.palette.navy,
  textAlign: "center",
  marginBottom: 5,
}

const COLLAGE_TEXT: TextStyle = {
  fontFamily: typography.regular,
  fontSize: 18,
  color: color.palette.navy,
  textAlign: "center",
  marginBottom: 10,
}

const BIO_TEXT: TextStyle = {
  fontFamily: typography.regular,
  fontSize: 14,
  color: color.palette.navy,
  textAlign: "center",
  marginLeft: 30,
  marginRight: 30,
}

const IMAGE_BACK: ImageStyle = {
  width: Dimensions.get("window").width,
  height: (Dimensions.get("window").width * 76) / 360,
  position: "relative",
  top: ((Dimensions.get("window").width * 76) / 360) * -1,
}

export const AppProfileShowScreen = observer(function AppProfileShowScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  const rootStore = useStores()
  const profile = rootStore.authStore.profile

  // Pull in navigation via hook
  // const navigation = useNavigation()

  const onEditProfile = () => {
    console.log("edit")
  }

  return (
    <Screen style={ROOT} preset="fixed">
      <NavigationBar center edit={onEditProfile} canBack title="appProfileShowScreen.title" />
      <Container>
        <Grid>
          <Row size={7}>
            <Col>
              <Image style={PROFILE_IMAGE} source={{ uri: profile.picture }} />
              <Image style={IMAGE_BACK} source={require("./img/bkgImage.png")} />
            </Col>
          </Row>
          <Row size={3}>
            <Col>
              <Row>
                <Col>
                  <Text style={USER_TEXT} text={`${profile.getNameWithAge()}`} />
                  <Text style={PROFESSION_TEXT} text={`${profile.profession}`} />
                  <Text style={COLLAGE_TEXT} text={`${profile.collage}`} />
                  <Text style={BIO_TEXT} text={`${profile.bio}`} />
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </Container>
    </Screen>
  )
})
