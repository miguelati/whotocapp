import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle } from "react-native"
import { Container, Grid, Row, Col } from "native-base"
import { Screen, NavigationBar, ChatEmpty } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

export const AppMessagesMainScreen = observer(function AppMessagesMainScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()
  // <NavigationBar title="appMessagesMain.title" />
  // Pull in navigation via hook
  const navigation = useNavigation()

  const onPress = () => {
    navigation.navigate("appDiscover")
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <Container>
        <Grid>
          <Row>
            <Col>
              <ChatEmpty onPress={onPress} />
            </Col>
          </Row>
        </Grid>
      </Container>
    </Screen>
  )
})
