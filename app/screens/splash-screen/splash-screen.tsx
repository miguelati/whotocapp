import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, ImageStyle, Image, ImageBackground } from "react-native"
import { Screen } from "../../components"
import { color } from "../../theme"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const BACKGROUND: ImageStyle = {
  width: '100%',
  height: '100%',
  alignItems: "center",
  justifyContent: "center"
}

const LOGO: ImageStyle = {
  width: 233,
  height: 180
}

export const SplashScreen = observer(function SplashScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="fixed" unsafe>
      <ImageBackground source={require('./img/bkg.png')} style={BACKGROUND}>
        <Image source={require('./img/logo.png')} style={LOGO}/>
      </ImageBackground>
    </Screen>
  )
})
