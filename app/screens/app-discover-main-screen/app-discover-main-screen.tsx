import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle } from "react-native"
import { Container, Grid, Row, Col } from "native-base"
import { Screen, NavigationBar, Radar } from "../../components"
// import { useNavigation } from "@react-navigation/native"
import { color } from "../../theme"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

export const AppDiscoverMainScreen = observer(function AppDiscoverMainScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="fixed">
      <NavigationBar center hasDiscovery title="appDiscoveryMain.title" />
      <Container>
        <Grid>
          <Row>
            <Col>
              <Radar />
            </Col>
          </Row>
        </Grid>
      </Container>
    </Screen>
  )
})
