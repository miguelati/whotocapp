import React from "react"
import { observer } from "mobx-react-lite"
import { ImageBackground, Image, ViewStyle, ImageStyle, TextStyle } from "react-native"
import * as Animatable from "react-native-animatable"
import { Screen, Text } from "../../components"
import { Container, Grid, Row, Col, View, Button } from "native-base"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, typography } from "../../theme"
export const imageBackground = require("./bg-wellcome.png")

export const AuthIntroScreen = observer(function AuthIntroScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  const users = [
    {
      id: "1",
      path: require("./users/user1.png"),
      style: { width: 126, height: 126 },
      styleView: { width: 165, height: 165, left: 0, top: 210 },
    },
    {
      id: "2",
      path: require("./users/user2.png"),
      style: { width: 32, height: 32 },
      styleView: { width: 41, height: 41, left: -16, top: 160 },
    },
    {
      id: "3",
      path: require("./users/user3.png"),
      style: { width: 64, height: 64 },
      styleView: { width: 83, height: 83, left: 44, top: 65 },
    },
    {
      id: "4",
      path: require("./users/user4.png"),
      style: { width: 32, height: 32 },
      styleView: { width: 41, height: 41, left: 156, top: 123 },
    },
    {
      id: "5",
      path: require("./users/user5.png"),
      style: { width: 64, height: 64 },
      styleView: { width: 83, height: 83, left: 165, top: 180 },
    },
    {
      id: "6",
      path: require("./users/user6.png"),
      style: { width: 32, height: 32 },
      styleView: { width: 41, height: 41, left: 229, top: 295 },
    },
    {
      id: "7",
      path: require("./users/user7.png"),
      style: { width: 64, height: 64 },
      styleView: { width: 83, height: 83, left: 225, top: 75 },
    },
    {
      id: "8",
      path: require("./users/user8.png"),
      style: { width: 32, height: 32 },
      styleView: { width: 41, height: 41, left: 350, top: 50 },
    },
    {
      id: "9",
      path: require("./users/user9.png"),
      style: { width: 126, height: 126 },
      styleView: { width: 165, height: 165, left: 300, top: 180 },
    },
  ]

  const CONTENT: ViewStyle = {
    flex: 1,
  }

  const CENTER: ViewStyle = {
    justifyContent: "center",
    alignItems: "center",
  }

  const IMAGE_BACKGROUND: ImageStyle = {
    width: "100%",
    height: "100%",
  }

  const VIEW_USER: ViewStyle = {
    position: "absolute",
    width: 160,
    height: 160,
    borderRadius: 80,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: color.radarWellcome,
  }

  const USER_STYLE: ImageStyle = {
    borderRadius: 63,
    width: 126,
    height: 126,
  }

  const VIEW_CONTAINER: ViewStyle = {
    height: 150,
    justifyContent: "space-between",
    marginLeft: 30,
    marginRight: 30,
  }

  const TITLE: TextStyle = {
    fontFamily: typography.bold,
    fontSize: 24,
    color: color.background,
  }

  const TEXT: TextStyle = {
    fontFamily: typography.semiBold,
    fontSize: 14,
    color: color.background,
  }

  const BUTTON: ViewStyle = {
    backgroundColor: color.background,
    borderRadius: 17,
    height: 46,
  }

  const BUTTON_TEXT: TextStyle = {
    fontFamily: typography.semiBold,
    color: color.buttonWhiteText,
    fontSize: 16,
  }

  const navigation = useNavigation()

  const gotoRegister = () => {
    navigation.navigate("authRegister")
  }

  return (
    <Screen preset="fixed" backgroundColor={color.wellcomeBackground}>
      <Container>
        <Animatable.View style={CONTENT}>
          <Grid>
            <Row>
              <Col style={CENTER}>
                <ImageBackground
                  resizeMode="cover"
                  source={imageBackground}
                  style={IMAGE_BACKGROUND}
                >
                  <Row size={2}>
                    <Col>
                      {users.map((user) => (
                        <View key={user.id + "3"} style={[VIEW_USER, user.styleView]}>
                          <Image
                            key={user.id}
                            source={user.path}
                            style={[USER_STYLE, user.style]}
                          />
                        </View>
                      ))}
                    </Col>
                  </Row>
                  <Row size={5}>
                    <Col style={CENTER}>
                      <View style={VIEW_CONTAINER}>
                        <View>
                          <Text style={TITLE} tx="authIntroScreen.title" />
                          <Text style={TEXT} tx="authIntroScreen.text" />
                        </View>
                        <Button style={BUTTON} block light onPress={gotoRegister}>
                          <Text style={BUTTON_TEXT} tx="authIntroScreen.button" />
                        </Button>
                      </View>
                    </Col>
                  </Row>
                </ImageBackground>
              </Col>
            </Row>
          </Grid>
        </Animatable.View>
      </Container>
    </Screen>
  )
})
