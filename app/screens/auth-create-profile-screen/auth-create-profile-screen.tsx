import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle, Dimensions, TouchableOpacity, Platform } from "react-native"
import { Container, Grid, Row, Col, Input, Item } from "native-base"
import DateTimePickerModal from "react-native-modal-datetime-picker"
import { Screen, Text, ButtonGradient, SelectCustom } from "../../components"
import { Item as SelectItem } from "../../components/select-custom/select-custom"
import { Formik, FormikHelpers } from "formik"
import moment from "moment"
import * as Yup from "yup"
import { useNavigation } from "@react-navigation/native"
import { useStores } from "../../models"
import { translate } from "../../i18n"
import { color, typography } from "../../theme"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  height: Dimensions.get("window").height,
}

const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 24,
  color: color.palette.orange,
  marginLeft: 30,
}

const TITLE_CONTAINER: ViewStyle = {
  justifyContent: "flex-end",
}

const SUBTITLE: TextStyle = {
  fontFamily: typography.medium,
  fontSize: 14,
  color: color.palette.navy,
  marginLeft: 30,
}

const INPUT_FIELD: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.palette.navy,
}

const FORM_CONTAINER: ViewStyle = {
  marginLeft: 30,
  marginRight: 30,
}

const MODAL_CONTAINER: ViewStyle = {
  height: 50,
}

interface Register {
  firstName: string
  lastName: string
  bornDate: Date
  sex?: "male" | "female"
  profession: string
  collage: string
}

export const AuthCreateProfileScreen = observer(function AuthCreateProfileScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  const rootStore = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  const [showPickerSex, setShowPickerSex] = useState(false)

  const [showPickerDate, setShowPickerDate] = useState(false)

  const _showDateTimePicker = () => {
    setShowPickerDate(!showPickerDate)
  }

  const _showSexPicker = () => {
    setShowPickerSex(!showPickerSex)
  }

  const _handleDatePicked = (date: Date, setFieldValue) => {
    _showDateTimePicker()
    setFieldValue("bornDate", date)
  }

  const _handleSexPicked = (item, setFieldValue) => {
    _showSexPicker()
    setFieldValue("sex", item)
  }

  const _handleSexClose = () => {
    _showSexPicker()
  }

  const validation = Yup.object().shape({
    firstName: Yup.string().required(
      translate("authCreateProfileScreen.fields.firstName.required"),
    ),
    lastName: Yup.string().required(translate("authCreateProfileScreen.fields.lastName.required")),
    bornDate: Yup.date().required(translate("authCreateProfileScreen.fields.bornDate.required")),
    sex: Yup.string().required(translate("authCreateProfileScreen.fields.sex.required")),
    profession: Yup.string().required(
      translate("authCreateProfileScreen.fields.profesion.required"),
    ),
    collage: Yup.string().required(translate("authCreateProfileScreen.fields.collage.required")),
  })

  const onRegisterSubmit = (values: Register, { setSubmitting }: FormikHelpers<Register>) => {
    console.log("Valido", values, setSubmitting)

    rootStore.authStore.createProfile({
      uid: "",
      firstName: values.firstName,
      lastName: values.lastName,
      bornDate: values.bornDate,
      sex: values.sex,
      profession: values.profession,
      collage: values.collage,
      bio: null,
      blockedUsers: undefined,
      lastUpdate: new Date(),
      os: Platform.OS === "ios" ? "ios" : "android",
      picture: null,
      picturePath: null,
      token: null,
      visible: true,
    })

    navigation.navigate("authProfilePicture")
  }

  const SexValues: SelectItem[] = [
    { name: translate("authCreateProfileScreen.fields.sex.values.male"), value: "male" },
    { name: translate("authCreateProfileScreen.fields.sex.values.female"), value: "female" },
  ]

  return (
    <Screen style={ROOT} preset="scroll">
      <Container>
        <Grid>
          <Row size={2} />
          <Row>
            <Col style={TITLE_CONTAINER}>
              <Text style={TITLE} tx="authCreateProfileScreen.title" />
              <Text style={SUBTITLE} tx="authCreateProfileScreen.subtitle" />
            </Col>
          </Row>
          <Row size={5} style={FORM_CONTAINER}>
            <Col>
              <Formik
                initialValues={{
                  firstName: "",
                  lastName: "",
                  bornDate: null,
                  sex: null,
                  profession: "",
                  collage: "",
                }}
                validationSchema={validation}
                onSubmit={onRegisterSubmit}
              >
                {({
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  values,
                  errors,
                  setFieldValue,
                  isSubmitting,
                }) => (
                  <>
                    <Row>
                      <Col>
                        <Item fixedLabel error={errors.firstName !== undefined}>
                          <Input
                            placeholder={translate("authCreateProfileScreen.fields.firstName.name")}
                            placeholderTextColor={color.palette.navy50}
                            style={INPUT_FIELD}
                            value={values.firstName}
                            onChangeText={handleChange("firstName")}
                            onBlur={handleBlur("firstName")}
                          />
                        </Item>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Item fixedLabel error={errors.lastName !== undefined}>
                          <Input
                            placeholder={translate("authCreateProfileScreen.fields.lastName.name")}
                            placeholderTextColor={color.palette.navy50}
                            style={INPUT_FIELD}
                            value={values.lastName}
                            onChangeText={handleChange("lastName")}
                            onBlur={handleBlur("lastName")}
                          />
                        </Item>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Item
                          style={MODAL_CONTAINER}
                          fixedLabel
                          error={errors.bornDate !== undefined}
                        >
                          <TouchableOpacity onPress={_showDateTimePicker}>
                            <Input
                              placeholder={translate(
                                "authCreateProfileScreen.fields.bornDate.name",
                              )}
                              placeholderTextColor={color.palette.navy50}
                              pointerEvents="none"
                              autoCapitalize="none"
                              editable={false}
                              style={INPUT_FIELD}
                              value={
                                values.bornDate == null
                                  ? ""
                                  : moment(values.bornDate).format("DD/MM/YYYY")
                              }
                            />
                          </TouchableOpacity>
                          <DateTimePickerModal
                            isVisible={showPickerDate}
                            onConfirm={(date) => _handleDatePicked(date, setFieldValue)}
                            onCancel={_showDateTimePicker}
                          />
                        </Item>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Item style={MODAL_CONTAINER} fixedLabel error={errors.sex !== undefined}>
                          <TouchableOpacity onPress={_showSexPicker}>
                            <Input
                              placeholder={translate("authCreateProfileScreen.fields.sex.name")}
                              placeholderTextColor={color.palette.navy50}
                              pointerEvents="none"
                              autoCapitalize="none"
                              editable={false}
                              style={INPUT_FIELD}
                              value={translate(
                                `authCreateProfileScreen.fields.sex.values.${values.sex}`,
                                { defaultValue: "" },
                              )}
                              onChangeText={handleChange("sex")}
                              onBlur={handleBlur("sex")}
                            />
                          </TouchableOpacity>
                          <SelectCustom
                            values={SexValues}
                            visible={showPickerSex}
                            onSelected={(item) => _handleSexPicked(item, setFieldValue)}
                            title="authCreateProfileScreen.sexOptionTitle"
                            onClose={_handleSexClose}
                          />
                        </Item>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Item fixedLabel error={errors.profession !== undefined}>
                          <Input
                            placeholder={translate(
                              "authCreateProfileScreen.fields.profession.name",
                            )}
                            placeholderTextColor={color.palette.navy50}
                            style={INPUT_FIELD}
                            value={values.profession}
                            onChangeText={handleChange("profession")}
                            onBlur={handleBlur("profession")}
                          />
                        </Item>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Item fixedLabel error={errors.collage !== undefined}>
                          <Input
                            placeholder={translate("authCreateProfileScreen.fields.collage.name")}
                            placeholderTextColor={color.palette.navy50}
                            style={INPUT_FIELD}
                            value={values.collage}
                            onChangeText={handleChange("collage")}
                            onBlur={handleBlur("collage")}
                          />
                        </Item>
                      </Col>
                    </Row>

                    <Row>
                      <Col>
                        <ButtonGradient
                          disabled={isSubmitting}
                          onPress={handleSubmit}
                          tx="authCreateProfileScreen.button"
                        />
                      </Col>
                    </Row>
                  </>
                )}
              </Formik>
            </Col>
          </Row>
          <Row size={2} />
        </Grid>
      </Container>
    </Screen>
  )
})
