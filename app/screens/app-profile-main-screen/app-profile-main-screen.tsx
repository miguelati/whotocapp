import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle, Image, ImageStyle, TouchableOpacity } from "react-native"
import { Container, Grid, Row, Col, View } from "native-base"
import { Screen, Text, Switch } from "../../components"
import { useNavigation } from "@react-navigation/native"
import { useStores } from "../../models"
import { color, typography } from "../../theme"
import { createIconSetFromFontello } from "react-native-vector-icons"
import fontelloConfig from "../../config/icons.json"
const IconFont = createIconSetFromFontello(fontelloConfig)

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const CENTER: ViewStyle = {
  alignItems: "center",
  justifyContent: "center",
}

const LEFT: ViewStyle = {
  alignItems: "flex-start",
  justifyContent: "center",
}

const HEADER_PROFILE: ViewStyle = {
  backgroundColor: color.palette.white,
  height: 100,
  shadowColor: color.palette.grey,
  shadowOffset: {
    width: 0,
    height: 10,
  },
  shadowOpacity: 0.5,
  shadowRadius: 2,
  elevation: 10,
}

const ICON_STYLE: TextStyle = {
  color: color.palette.navy,
  fontSize: 24,
}

const IMAGE_STYLE: ImageStyle = {
  width: 64,
  height: 64,
  borderRadius: 32,
}

const PROFILE_NAME: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 18,
  color: color.palette.navy,
}

const LINK_SHOW_PROFILE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.palette.orange,
}

const ROW_HEADER: ViewStyle = {
  height: 100,
}

const ROW_TITLE: ViewStyle = {
  height: 50,
  marginLeft: 30,
  marginRight: 30,
  alignItems: "flex-end",
  borderBottomWidth: 1,
  borderColor: color.palette.greyLight,
}

const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 12,
  color: color.palette.navy50,
  marginBottom: 10,
}

const OPTION: TextStyle = {
  fontFamily: typography.medium,
  fontSize: 16,
  color: color.palette.navy,
}

const ROW_OPTION: ViewStyle = {
  height: 60,
  marginLeft: 30,
  marginRight: 30,
  borderBottomWidth: 1,
  borderColor: color.palette.greyLight,
}

const LOGOUT_ICON: TextStyle = {
  color: color.palette.shine100,
  fontSize: 24,
}

const LOGOUT_TEXT: TextStyle = {
  fontFamily: typography.medium,
  fontSize: 16,
  color: color.palette.shine100,
}

export const AppProfileMainScreen = observer(function AppProfileMainScreen() {
  const navigation = useNavigation()
  const rootStore = useStores()
  const profile = rootStore.authStore.profile
  const onPressViewProfile = () => {
    navigation.navigate("appProfileShowScreen")
  }

  const onLogoutPress = async () => {
    console.log("logout")
    await rootStore.authStore.signOut()
  }

  return (
    <Screen style={ROOT} preset="fixed">
      <Container>
        <Grid>
          <Row style={ROW_HEADER}>
            <Col>
              <View style={HEADER_PROFILE}>
                <Row>
                  <Col size={2} style={CENTER}>
                    <Image
                      source={{ uri: profile != null ? profile.picture : null }}
                      style={IMAGE_STYLE}
                    />
                  </Col>
                  <Col size={3} style={LEFT}>
                    <Text
                      style={PROFILE_NAME}
                      text={profile != null ? profile.getFullName() : ""}
                    />
                    <TouchableOpacity onPress={onPressViewProfile}>
                      <Text style={LINK_SHOW_PROFILE} tx="appProfileMainScreen.showProfile" />
                    </TouchableOpacity>
                  </Col>
                  <Col style={CENTER}>
                    <Image source={require("./img/next.png")} />
                  </Col>
                </Row>
              </View>
            </Col>
          </Row>
          <Row style={ROW_TITLE}>
            <Col>
              <Text style={TITLE} tx="appProfileMainScreen.configuration.title" />
            </Col>
          </Row>
          <Row style={ROW_OPTION}>
            <Col size={1} style={LEFT}>
              <IconFont style={ICON_STYLE} name="block" />
            </Col>
            <Col size={5} style={LEFT}>
              <Text style={OPTION} tx="appProfileMainScreen.configuration.bannedUsers" />
            </Col>
            <Col size={1} style={CENTER}>
              <Image source={require("./img/next.png")} />
            </Col>
          </Row>
          <Row style={ROW_OPTION}>
            <Col size={1} style={LEFT}>
              <IconFont style={ICON_STYLE} name="share" />
            </Col>
            <Col size={6} style={LEFT}>
              <Text style={OPTION} tx="appProfileMainScreen.configuration.shareWhotoc" />
            </Col>
          </Row>
          <Row style={ROW_TITLE}>
            <Col>
              <Text style={TITLE} tx="appProfileMainScreen.notification.title" />
            </Col>
          </Row>
          <Row style={ROW_OPTION}>
            <Col size={1} style={LEFT}>
              <IconFont style={ICON_STYLE} name="notification" />
            </Col>
            <Col size={5} style={LEFT}>
              <Text style={OPTION} tx="appProfileMainScreen.notification.newToc" />
            </Col>
            <Col size={1} style={CENTER}>
              <Switch value={true} />
            </Col>
          </Row>
          <Row style={ROW_OPTION}>
            <Col size={1} style={LEFT}>
              <IconFont style={ICON_STYLE} name="notification" />
            </Col>
            <Col size={5} style={LEFT}>
              <Text style={OPTION} tx="appProfileMainScreen.notification.newMessage" />
            </Col>
            <Col size={1} style={CENTER}>
              <Switch value={false} />
            </Col>
          </Row>
          <Row style={ROW_OPTION}>
            <Col size={1} style={LEFT}>
              <IconFont style={ICON_STYLE} name="notification" />
            </Col>
            <Col size={5} style={LEFT}>
              <Text style={OPTION} tx="appProfileMainScreen.notification.alertProximity" />
            </Col>
            <Col size={1} style={CENTER}>
              <Switch value={true} />
            </Col>
          </Row>
          <Row style={ROW_TITLE}>
            <Col>
              <Text style={TITLE} tx="appProfileMainScreen.configuration.title" />
            </Col>
          </Row>
          <TouchableOpacity onPress={onLogoutPress}>
            <Row style={ROW_OPTION}>
              <Col size={1} style={LEFT}>
                <IconFont style={LOGOUT_ICON} name="logout" />
              </Col>
              <Col size={6} style={LEFT}>
                <Text style={LOGOUT_TEXT} tx="appProfileMainScreen.logout" />
              </Col>
            </Row>
          </TouchableOpacity>
          <Row size={2} />
        </Grid>
      </Container>
    </Screen>
  )
})
