import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle, Image } from "react-native"
import { Container, Grid, Row, Col } from "native-base"
import { Screen, Text, ButtonGradient } from "../../components"
import { useNavigation } from "@react-navigation/native"
import { color, typography } from "../../theme"
import background from "../../../assets/images/auth-welcome/logo.png"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const CENTER: ViewStyle = {
  alignItems: "center",
  justifyContent: "center",
}

const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 28,
  color: color.palette.orange,
  textAlign: "center",
}

const TITLE_CONTAINER: ViewStyle = {
  alignItems: "center",
  justifyContent: "flex-end",
}

const DESCRIPTION: TextStyle = {
  fontFamily: typography.medium,
  fontSize: 14,
  color: color.palette.navy,
  textAlign: "center",
}

const DESCRIPTION_CONTAINER: ViewStyle = {
  alignItems: "center",
  justifyContent: "flex-start",
  marginLeft: 40,
  marginRight: 40,
  padding: 10,
}

export const AuthWelcomeScreen = observer(function AuthWelcomeScreen() {
  const navigation = useNavigation()
  const onPressContinue = () => {
    navigation.navigate("authCreateProfile")
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <Container>
        <Grid>
          <Row size={4}>
            <Col style={CENTER}>
              <Image source={background} />
            </Col>
          </Row>
          <Row size={1}>
            <Col style={TITLE_CONTAINER}>
              <Text style={TITLE} tx="authWelcomeScreen.title" />
            </Col>
          </Row>
          <Row size={1}>
            <Col style={DESCRIPTION_CONTAINER}>
              <Text style={DESCRIPTION} tx="authWelcomeScreen.description" />
            </Col>
          </Row>
          <Row>
            <Col>
              <ButtonGradient onPress={onPressContinue} tx="authWelcomeScreen.button" />
            </Col>
          </Row>
        </Grid>
      </Container>
    </Screen>
  )
})
