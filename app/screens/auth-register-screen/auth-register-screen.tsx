import * as React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle } from "react-native"
import { Container, Grid, Row, Col } from "native-base"
import { Formik, FormikHelpers } from "formik"
import * as Yup from "yup"
import { translate } from "../../i18n"
import { Screen, Text, NavigationBar, PhoneInput, Checkbox, ButtonGradient } from "../../components"
import { useNavigation } from "@react-navigation/native"
import { color, typography } from "../../theme"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
}

const TITLE_CONTAINER: ViewStyle = {
  height: 50,
  alignItems: "flex-end",
}

const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 24,
  color: color.primary,
  marginLeft: 30,
}

const PHONE_CONTAINER: ViewStyle = {
  height: 105,
  alignItems: "center",
}

const CHECKBOX_CONTAINER: ViewStyle = {
  paddingLeft: 30,
  height: 40,
  alignItems: "center",
}

const BUTTON_CONTAINER: ViewStyle = {
  height: 90,
}

const ERRORS_CONTAINER: ViewStyle = {
  height: 45,
  justifyContent: "center",
}

const TEXT_ERROR: TextStyle = {
  textAlign: "center",
  fontFamily: typography.bold,
  fontSize: 12,
  color: color.palette.red,
}

export interface Register {
  phone: string
  phoneCode: string
  check: boolean
}

export const AuthRegisterScreen = observer(function AuthRegisterScreen() {
  const navigation = useNavigation()

  const onRegisterSubmit = (values: Register, { setSubmitting }: FormikHelpers<Register>) => {
    setSubmitting(true)
    const phone = values.phone.replace(/^0/, "")
    navigation.navigate("authVerify", { phoneNumber: `${values.phoneCode}${phone}` })
    setSubmitting(false)
  }

  const validation = Yup.object().shape({
    phone: Yup.string()
      .required(translate("authRegisterScreen.validation.phone.required"))
      .matches(/^[0-9]+$/, translate("authRegisterScreen.validation.phone.match")),
    phoneCode: Yup.string()
      .required(translate("authRegisterScreen.validation.phoneCode.required"))
      .matches(/^\+[0-9]{1,4}$/, translate("authRegisterScreen.validation.phoneCode.match")),
    check: Yup.boolean().equals([true], translate("authRegisterScreen.validation.terms.equals")),
  })

  return (
    <Screen style={ROOT} preset="scroll">
      <Container>
        <NavigationBar canBack />
        <Grid style={ROOT}>
          <Row style={TITLE_CONTAINER}>
            <Col>
              <Text style={TITLE} tx="authRegisterScreen.title" />
            </Col>
          </Row>
          <Formik
            initialValues={{
              phone: "",
              phoneCode: "+595",
              check: false,
            }}
            validationSchema={validation}
            onSubmit={onRegisterSubmit}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
              setFieldValue,
              isSubmitting,
            }) => (
              <>
                <Row style={PHONE_CONTAINER}>
                  <Col>
                    <PhoneInput
                      phone={values.phone}
                      phoneCode={values.phoneCode}
                      onBlur={(e: any) => {
                        handleBlur("phone")(e)
                        handleBlur("phoneCode")(e)
                      }}
                      onChangeText={(phone: string, phoneCode: string) => {
                        handleChange("phone")(phone)
                        handleChange("phoneCode")(phoneCode)
                      }}
                    />
                  </Col>
                </Row>
                <Row style={CHECKBOX_CONTAINER}>
                  <Col>
                    <Checkbox
                      value={values.check}
                      tx="authRegisterScreen.acept"
                      onToggle={(value: boolean) => {
                        setFieldValue("check", value)
                      }}
                    />
                  </Col>
                </Row>
                <Row style={ERRORS_CONTAINER}>
                  <Col>
                    {errors.phone && <Text style={TEXT_ERROR} text={errors.phone} />}
                    {errors.check && <Text style={TEXT_ERROR} text={errors.check} />}
                  </Col>
                </Row>
                <Row style={BUTTON_CONTAINER}>
                  <Col>
                    <ButtonGradient
                      disabled={isSubmitting}
                      onPress={handleSubmit}
                      tx="authRegisterScreen.button"
                    />
                  </Col>
                </Row>
              </>
            )}
          </Formik>
        </Grid>
      </Container>
    </Screen>
  )
})
