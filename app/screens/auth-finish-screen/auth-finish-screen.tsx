import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle } from "react-native"
import { useNavigation } from "@react-navigation/native"
import { Container, Grid, Row, Col, Textarea } from "native-base"
import { Formik } from "formik"
import * as Yup from "yup"

import { Screen, Text, ButtonGradient, Hud, NavigationBar } from "../../components"
import { translate } from "../../i18n"

import { useStores } from "../../models"
import { color, typography } from "../../theme"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 24,
  color: color.palette.orange,
}

const SUBTITLE: TextStyle = {
  fontFamily: typography.medium,
  fontSize: 14,
  color: color.palette.navy,
  marginRight: 110,
}

const TITLE_CONTAINER: ViewStyle = {
  marginLeft: 30,
}

const TEXTAREA: TextStyle = {
  borderWidth: 0,
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.palette.navy,
  marginLeft: 30,
  marginRight: 30,
}

const ERROR_TEXT: TextStyle = {
  textAlign: "center",
  fontFamily: typography.bold,
  fontSize: 12,
  color: color.palette.red,
}

export const AuthFinishScreen = observer(function AuthFinishScreen() {
  const rootStore = useStores()
  const navigation = useNavigation()

  const [loading, setLoading] = React.useState(false)

  const validation = Yup.object().shape({
    bio: Yup.string().required(translate("authFinish.validation")),
  })

  const onCompleteLater = () => {
    navigation.navigate("appNavigator")
  }

  const onFinish = async ({ bio }, { setSubmitting }) => {
    setLoading(true)
    setSubmitting(true)
    try {
      await rootStore.authStore.addBio(bio)
      navigation.navigate("appNavigator")
      setLoading(false)
      setSubmitting(false)
    } catch (error) {
      setLoading(false)
      setSubmitting(false)
    }
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <Container>
        <NavigationBar completeLater={onCompleteLater} />
        <Grid>
          <Row />
          <Row>
            <Col style={TITLE_CONTAINER}>
              <Text style={TITLE} tx="authFinish.title" />
              <Text style={SUBTITLE} tx="authFinish.subtitle" />
            </Col>
          </Row>
          <Formik
            initialValues={{
              bio: "",
            }}
            validationSchema={validation}
            onSubmit={onFinish}
          >
            {({ handleChange, handleBlur, handleSubmit, values, errors, isSubmitting }) => (
              <>
                <Row size={3}>
                  <Col>
                    <Textarea
                      style={TEXTAREA}
                      value={values.bio}
                      rowSpan={5}
                      bordered={false}
                      underline={false}
                      onChangeText={handleChange("bio")}
                      onBlur={handleBlur("bio")}
                      placeholder={translate("authFinish.placeholder")}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    {errors.bio !== undefined && (
                      <Text style={ERROR_TEXT} text={errors.bio as string} />
                    )}
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <ButtonGradient
                      disabled={isSubmitting}
                      onPress={handleSubmit}
                      tx="authFinish.button"
                    />
                  </Col>
                </Row>
              </>
            )}
          </Formik>
          <Row size={5} />
          <Hud isVisible={loading} loading={true} />
        </Grid>
      </Container>
    </Screen>
  )
})
