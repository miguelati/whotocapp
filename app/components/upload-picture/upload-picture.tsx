import * as React from "react"
import { TextStyle, View, ViewStyle, ImageStyle, Image, TouchableOpacity } from "react-native"
import ImagePicker from "react-native-image-crop-picker"
import { Actionsheet, ActionsheetOption } from "../actionsheet/actionsheet"
import { color, typography } from "../../theme"
import { Text } from "../text/text"

const CONTAINER: ViewStyle = {
  alignItems: "flex-end",
}

const TEXT: TextStyle = {
  fontFamily: typography.semiBold,
  fontSize: 16,
  color: color.palette.orange,
  alignItems: "center",
}

const UPLOAD_BUTTON: ViewStyle = {
  alignItems: "center",
  position: "relative",
  width: 80,
  top: -75,
}

const PROFILE_IMAGE: ImageStyle = {
  width: 222,
  height: 222,
  borderRadius: 111,
  borderColor: color.palette.orange30,
}

const BORDER_IMAGE: ViewStyle = {
  width: 250,
  height: 250,
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: color.palette.orange30,
  borderRadius: 125,
}

export interface UploadPictureProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  onPictureChanged?: (image) => void
}

/**
 * Describe your component here
 */
export function UploadPicture(props: UploadPictureProps) {
  const { style, onPictureChanged } = props

  const [showActionsheet, setShowActionsheet] = React.useState(false)
  const [image, setImate] = React.useState(null)

  const onPressUpload = () => {
    setShowActionsheet(true)
  }

  const pictureOptions = {
    width: 1024,
    height: 1024,
    compressImageMaxWidth: 1024,
    compressImageMaxHeight: 1024,
    avoidEmptySpaceAroundImage: true,
    cropping: true,
    cropperCircleOverlay: true,
  }

  const onClose = () => {
    setShowActionsheet(false)
  }

  const setImage = (image) => {
    setShowActionsheet(false)
    setImate({
      uri: image.path,
      width: image.width,
      height: image.height,
    })
    onPictureChanged(image)
  }

  const onSelected = async (option) => {
    try {
      setImage(
        option.id === "1"
          ? await ImagePicker.openPicker({ ...pictureOptions, mediaType: "photo" })
          : await ImagePicker.openCamera(pictureOptions),
      )
    } catch (error) {
      setShowActionsheet(false)
    }
  }

  const options: ActionsheetOption[] = [
    { id: "1", text: "uploadPicture.choosePicture", type: "option" },
    { id: "2", text: "uploadPicture.takePicture", type: "option" },
  ]

  return (
    <View style={[CONTAINER, style]}>
      {image === null && <Image source={require("./images/placeholder.png")} />}
      {image !== null && (
        <View style={BORDER_IMAGE}>
          <Image style={PROFILE_IMAGE} source={image} />
        </View>
      )}
      <TouchableOpacity style={UPLOAD_BUTTON} onPress={onPressUpload}>
        <Image source={require("./images/upload-button.png")} />
        <Text style={TEXT} tx="uploadPicture.upload" />
      </TouchableOpacity>
      <Actionsheet
        isVisible={showActionsheet}
        options={options}
        onClose={onClose}
        onSelected={onSelected}
      />
    </View>
  )
}
