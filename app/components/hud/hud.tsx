import * as React from "react"
import { TextStyle, View, ViewStyle, ActivityIndicator } from "react-native"
import Modal from "react-native-modal"
import { Row, Col, Icon } from "native-base"
import { color, typography } from "../../theme"
import { Text } from "../text/text"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  width: 220,
  height: 270,
  alignItems: "center",
  borderRadius: 17,
  backgroundColor: color.palette.white,
}

const TITLE_TEXT: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 18,
  color: color.palette.red,
  textAlign: "center",
  marginLeft: 30,
  marginRight: 30,
}

const DESCRIPTION_TEXT: TextStyle = {
  fontFamily: typography.regular,
  fontSize: 18,
  color: color.palette.navy,
  textAlign: "center",
  marginLeft: 30,
  marginRight: 30,
}

const CENTER: ViewStyle = {
  justifyContent: "center",
  alignItems: "center",
}

export interface HudProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  isVisible?: boolean

  title?: string

  description?: string

  icon?: string

  loading?: boolean
}

/**
 * Describe your component here
 */
export function Hud(props: HudProps) {
  const { style, isVisible, title, description, icon, loading } = props

  const messageToShow = description || "hud.description"
  const titleToShow = title || "hud.title"

  return (
    <Modal isVisible={isVisible} style={CENTER}>
      <View style={[CONTAINER, style]}>
        {icon && (
          <Row>
            <Col style={CENTER}>
              <Icon name={icon} type="Entypo" />
            </Col>
          </Row>
        )}
        {loading && (
          <Row>
            <Col style={CENTER}>
              <ActivityIndicator size="large" />
            </Col>
          </Row>
        )}
        <Row>
          <Col>
            <Text style={TITLE_TEXT} tx={titleToShow} />
            <Text style={DESCRIPTION_TEXT} tx={messageToShow} />
          </Col>
        </Row>
      </View>
    </Modal>
  )
}
