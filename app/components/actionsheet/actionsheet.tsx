import * as React from "react"
import { TextStyle, View, ViewStyle } from "react-native"
import { Row, Col, Button, Icon } from "native-base"
import Modal from "react-native-modal"
import { color, typography } from "../../theme"
import { Text } from "../text/text"

const CONTAINER: ViewStyle = {
  justifyContent: "flex-end",
  margin: 0,
}

const VIEW: ViewStyle = {
  width: "100%",
  height: 300,
  backgroundColor: "#FFF",
  justifyContent: "space-around",
}

const MARGIN_TOP_BOTTOM: ViewStyle = {
  height: 15,
}

const BUTTON_CONTAINER: ViewStyle = {
  height: 50,
}

const OPTION_BUTTON: ViewStyle = {
  marginLeft: 30,
  marginRight: 30,
  borderRadius: 17,
  borderColor: color.palette.navy,
}

const OPTION_TEXT: TextStyle = {
  fontFamily: typography.semiBold,
  fontSize: 16,
  color: color.palette.navy,
}

const ACTIVE_BUTTON: ViewStyle = {
  marginLeft: 30,
  marginRight: 30,
  borderRadius: 17,
  borderColor: color.palette.red,
}

const ACTIVE_TEXT: TextStyle = {
  fontFamily: typography.semiBold,
  fontSize: 16,
  color: color.palette.red,
}

const CANCEL_BUTTON: ViewStyle = {
  marginLeft: 30,
  marginRight: 30,
  borderRadius: 17,
  backgroundColor: color.palette.navy,
}

const CANCEL_TEXT: TextStyle = {
  fontFamily: typography.semiBold,
  fontSize: 16,
  color: color.palette.white,
}

export interface ActionsheetOption {
  type: "active" | "option"
  icon?: string
  text: string
  id: string
}

export interface ActionsheetProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  isVisible?: boolean

  options: ActionsheetOption[]

  onClose?: () => void
  onSelected?: (option: ActionsheetOption) => void
}

/**
 * Describe your component here
 */
export function Actionsheet(props: ActionsheetProps) {
  const { style, isVisible, options, onClose, onSelected } = props

  const totalHeight = (options.length + 3) * 50 + 30

  return (
    <Modal isVisible={isVisible} style={[CONTAINER, style]}>
      <View style={[VIEW, { height: totalHeight }]}>
        <Row style={MARGIN_TOP_BOTTOM} />
        {options.map((option) => (
          <Row key={option.id} style={BUTTON_CONTAINER}>
            <Col>
              <Button
                bordered
                block
                style={option.type === "option" ? OPTION_BUTTON : ACTIVE_BUTTON}
                onPress={() => onSelected(option)}
              >
                {option.icon && <Icon name={option.icon} type="Entypo" />}
                <Text
                  style={option.type === "option" ? OPTION_TEXT : ACTIVE_TEXT}
                  tx={option.text}
                />
              </Button>
            </Col>
          </Row>
        ))}

        <Row style={BUTTON_CONTAINER}>
          <Col>
            <Button block style={CANCEL_BUTTON} onPress={onClose}>
              <Text style={CANCEL_TEXT} tx="actionsheet.cancel" />
            </Button>
          </Col>
        </Row>
        <Row style={MARGIN_TOP_BOTTOM} />
      </View>
    </Modal>
  )
}
