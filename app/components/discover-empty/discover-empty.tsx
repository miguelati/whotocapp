import * as React from "react"
import { TextStyle, View, ViewStyle, ImageBackground, Image } from "react-native"
import { Row, Col } from "native-base"
import { color, typography } from "../../theme"
import { Text } from "../text/text"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  flex: 1,
}

const IMAGE_CONTAINER: ViewStyle = {
  justifyContent: "flex-end",
  alignItems: "center",
}

const TEXT_CONTAINER: ViewStyle = {
  justifyContent: "flex-start",
  alignItems: "center",
}

const BACKGROUND: ViewStyle = {
  width: "100%",
  height: "100%",
}

const TITLE: TextStyle = {
  marginTop: 50,
  fontFamily: typography.bold,
  fontSize: 24,
  color: color.palette.orange,
}

const DESCRIPTION: TextStyle = {
  marginTop: 20,
  marginLeft: 80,
  marginRight: 80,
  textAlign: "center",
  fontFamily: typography.medium,
  fontSize: 14,
  color: color.palette.navy,
}

export interface DiscoverEmptyProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
}

/**
 * Describe your component here
 */
export function DiscoverEmpty(props: DiscoverEmptyProps) {
  const { style } = props

  return (
    <View style={[CONTAINER, style]}>
      <ImageBackground style={BACKGROUND} source={require("./img/back.png")}>
        <Row>
          <Col style={IMAGE_CONTAINER}>
            <Image source={require("./img/graphic.png")} />
          </Col>
        </Row>
        <Row>
          <Col style={TEXT_CONTAINER}>
            <Text style={TITLE} tx="discoverEmpty.title" />
            <Text style={DESCRIPTION} tx="discoverEmpty.description" />
          </Col>
        </Row>
      </ImageBackground>
    </View>
  )
}
