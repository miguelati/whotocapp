import * as React from "react"
import { TextStyle, ViewStyle, View, ImageBackground, TouchableNativeFeedback } from "react-native"
import { color } from "../../theme"
import * as Animatable from "react-native-animatable"
import { createIconSetFromFontello } from "react-native-vector-icons"
import fontelloConfig from "../../config/icons.json"
import ProgressCircle from "react-native-progress-circle"
const IconFont = createIconSetFromFontello(fontelloConfig)

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  width: 90,
  height: 90,
  backgroundColor: color.palette.white,
  borderRadius: 45,
  alignItems: "center",
  shadowColor: color.palette.shine70,
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity: 1.0,
  shadowRadius: 20,
  elevation: 24,
}

const CONTAINER_SHADOW: ViewStyle = {
  justifyContent: "center",
  width: 90,
  height: 90,
  borderRadius: 45,
  alignItems: "center",
  shadowColor: color.palette.black,
  shadowOffset: {
    width: 0,
    height: 10,
  },
  shadowOpacity: 0.3,
  shadowRadius: 4,
  elevation: 10,
}

const BACKGROUND: ViewStyle = {
  width: 80,
  height: 80,
  alignItems: "center",
  justifyContent: "center",
}

const ICON_STYLE: TextStyle = {
  fontSize: 70,
  color: color.palette.white,
}

const PROGRESS_IN: ViewStyle = {
  backgroundColor: color.palette.white,
  width: 100,
  height: 100,
  borderRadius: 50,
  alignItems: "center",
  justifyContent: "center",
}

const ICON_PROGRESS_STYLE: TextStyle = {
  fontSize: 40,
  color: color.palette.orange,
}

const PROGRESS: ViewStyle = {
  shadowColor: color.palette.shine70,
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity: 1.0,
  shadowRadius: 20,
  elevation: 24,
}

export interface TocButtonProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  tocStatus?: boolean
}

/**
 * Describe your component here
 */
export function TocButton(props: TocButtonProps) {
  const { style, tocStatus } = props

  const [buttonState, setButtonState] = React.useState("normal")
  const [tocStatusState, setTocStatusState] = React.useState(tocStatus == null ? false : tocStatus)
  let container = null
  let shadow = null

  const handleContainerRef = (ref) => (container = ref)
  const handleShadowRef = (ref) => (shadow = ref)

  const reset = () => {
    console.log(buttonState)
    if (buttonState === "toc") {
      container.transitionTo({
        shadowColor: color.palette.shine70,
        shadowOpacity: 1.0,
        shadowRadius: 20,
        elevation: 24,
      })
      shadow.transitionTo({
        shadowOpacity: 0.3,
        shadowRadius: 4,
        elevation: 10,
      })
    }
  }

  const animationEnd = () => {
    if (buttonState === "tocToc") {
      setTocStatusState(true)
    }
  }

  const onPress = () => {
    if (buttonState === "normal") {
      setButtonState("toc")
      container.transitionTo({
        shadowColor: color.palette.shine70,
        shadowOpacity: 1.0,
        shadowRadius: 20,
        elevation: 10,
      })
      shadow.transitionTo({
        shadowOpacity: 0.0,
        shadowRadius: 0,
        elevation: 0,
      })
    } else if (buttonState === "toc") {
      shadow.animate(
        {
          0: {
            backgroundColor: color.palette.white,
            width: 90,
            height: 90,
            borderRadius: 45,
          },
          0.9: {
            backgroundColor: color.palette.orange30,
            width: 100,
            height: 100,
            borderRadius: 50,
          },
          1: {
            backgroundColor: color.palette.orange30,
            width: 130,
            height: 130,
            borderRadius: 65,
          },
        },
        1000,
      )

      setButtonState("tocToc")
    }
  }

  return (
    <>
      {tocStatusState === false && (
        <TouchableNativeFeedback onPress={onPress}>
          <Animatable.View style={CONTAINER_SHADOW} ref={handleContainerRef}>
            <Animatable.View
              duration={3000}
              onTransitionEnd={animationEnd}
              style={[CONTAINER, style]}
              ref={handleShadowRef}
            >
              <ImageBackground style={BACKGROUND} source={require("./img/bkgNormal.png")}>
                <IconFont name="whotoc-mini" style={ICON_STYLE} />
              </ImageBackground>
            </Animatable.View>
          </Animatable.View>
        </TouchableNativeFeedback>
      )}
      {tocStatusState && (
        <View style={PROGRESS}>
          <ProgressCircle
            percent={30}
            radius={50}
            borderWidth={6}
            color={color.palette.greyLight}
            shadowColor={color.palette.orange}
            bgColor={color.palette.greyLight}
          >
            <View style={PROGRESS_IN}>
              <IconFont name="chat" style={ICON_PROGRESS_STYLE} />
            </View>
          </ProgressCircle>
        </View>
      )}
    </>
  )
}
