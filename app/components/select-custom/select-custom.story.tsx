import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { color } from "../../theme"
import { SelectCustom } from "./select-custom"

storiesOf("SelectCustom", module)
  .addDecorator((fn) => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <SelectCustom visible={true} values={[]} style={{ backgroundColor: color.error }} />
      </UseCase>
    </Story>
  ))
