import * as React from "react"
import { TextStyle, View, ViewStyle, ImageBackground, TouchableOpacity } from "react-native"
import resolveAssetSource from "react-native/Libraries/Image/resolveAssetSource"
import { color, typography } from "../../theme"
import { Text } from "../text/text"

const background = require("../../../assets/images/gradient-button/large.png")
const imageSize = resolveAssetSource(background)

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  alignItems: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.semiBold,
  fontSize: 16,
  color: color.palette.white,
}

const OPACITY: ViewStyle = {
  opacity: 0.5,
}

const BACKGROUND: ViewStyle = {
  height: imageSize.height,
  width: imageSize.width,
  alignItems: "center",
  justifyContent: "center",
}

export interface ButtonGradientProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  tx?: string

  text?: string

  disabled?: boolean

  onPress?: () => void
}

/**
 * Describe your component here
 */
export function ButtonGradient(props: ButtonGradientProps) {
  const { style, tx, text, disabled, onPress } = props

  return (
    <View style={[CONTAINER, style]}>
      <TouchableOpacity disabled={disabled} onPress={onPress}>
        <ImageBackground source={background} style={[BACKGROUND, disabled ? OPACITY : null]}>
          <Text style={TEXT} tx={tx} text={text} />
        </ImageBackground>
      </TouchableOpacity>
    </View>
  )
}
