import * as React from "react"
import { View, ViewStyle, TextStyle, TouchableOpacity } from "react-native"
import { color } from "../../theme"

import { createIconSetFromFontello } from "react-native-vector-icons"
import fontelloConfig from "../../config/icons.json"
const IconFont = createIconSetFromFontello(fontelloConfig)

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  alignItems: "center",
  width: 40,
  height: 40,
  borderRadius: 20,
  shadowColor: color.palette.green,
  shadowOffset: {
    width: 0,
    height: 3,
  },
  backgroundColor: color.palette.white,
  shadowOpacity: 0.71,
  shadowRadius: 5,

  elevation: 14,
}

const DISCOVERY_ICON: TextStyle = {
  fontSize: 24,
  color: color.palette.green,
  textAlign: "center",
}

export interface PowerButtonProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  onPress: () => void
}

/**
 * Describe your component here
 */
export function PowerButton(props: PowerButtonProps) {
  const { style, onPress } = props

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[CONTAINER, style]}>
        <IconFont name="power" style={DISCOVERY_ICON} />
      </View>
    </TouchableOpacity>
  )
}
