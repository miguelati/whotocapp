import * as React from "react"
import { TextStyle, Image, ViewStyle } from "react-native"
import { Row, Col } from "native-base"
import { color, typography } from "../../theme"
import { Text } from '../text/text'
import { ButtonGradient } from "../button-gradient/button-gradient"

const CENTER: ViewStyle = {
  alignItems: "center",
  justifyContent: "center",
  marginLeft: 50,
  marginRight: 50,
}

const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 36,
  color: color.palette.orange,
}

const SUBTITLE: TextStyle = {
  fontFamily: typography.medium,
  fontSize: 18,
  color: color.palette.navy,
  textAlign: "center",
}

export interface ChatEmptyProps {
  /**
   * An optional style override useful for padding & margin.
   */
  onPress?: () => void
}

/**
 * Describe your component here
 */
export function ChatEmpty(props: ChatEmptyProps) {
  const { onPress } = props

  return (
    <>
      <Row>
        <Col style={CENTER}>
          <Text style={TITLE} tx="chatEmpty.title" />
        </Col>
      </Row>
      <Row>
        <Col style={CENTER}>
          <Text style={SUBTITLE} tx="chatEmpty.subTitle" />
        </Col>
      </Row>
      <Row size={4}>
        <Col style={CENTER}>
          <Image source={require("./img/bak.png")} />
        </Col>
      </Row>
      <Row>
        <Col style={CENTER}>
          <Text style={SUBTITLE} tx="chatEmpty.description" />
        </Col>
      </Row>
      <Row>
        <Col style={CENTER}>
          <ButtonGradient onPress={onPress} tx="chatEmpty.button" />
        </Col>
      </Row>
    </>
  )
}
