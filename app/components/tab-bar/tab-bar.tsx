import * as React from "react"
import { TextStyle, ViewStyle } from "react-native"
import { FooterTab, Footer, Button } from "native-base"
import { observer } from "mobx-react-lite"
import { color } from "../../theme"
import { BottomTabBarProps } from "@react-navigation/bottom-tabs"

import { createIconSetFromFontello } from "react-native-vector-icons"
import fontelloConfig from "../../config/icons.json"
const IconFont = createIconSetFromFontello(fontelloConfig)

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  backgroundColor: color.palette.white,
  borderWidth: 0,
  borderColor: color.palette.white,
  shadowColor: color.palette.orange,
  shadowOffset: {
    width: 10,
    height: -3,
  },
  shadowOpacity: 0.11,
  shadowRadius: 3,
}

const TABBAR: ViewStyle = {
  backgroundColor: color.palette.white,
}

const MARGIN_LEFT: ViewStyle = {
  marginLeft: 50,
}

const MARGIN_RIGHT: ViewStyle = {
  marginRight: 50,
}

const ICON: TextStyle = {
  fontSize: 30,
}

/**
 * Describe your component here
 */
export const TabBar = observer(function TabBar({
  state,
  descriptors,
  navigation,
}: BottomTabBarProps) {
  return (
    <Footer style={CONTAINER}>
      <FooterTab style={TABBAR}>
        {state.routes.map((route, index) => {
          let iconName = ""
          if (route.name === "appProfile") {
            iconName = "profile"
          } else if (route.name === "appDiscover") {
            iconName = "radar"
          } else if (route.name === "appMessages") {
            iconName = "chat"
          }

          const isFocused = state.index === index
          const iconStyle = [
            ICON,
            isFocused ? { color: color.palette.navy } : { color: color.palette.navy30 },
          ]

          let marginStyle = null
          if (index === 0) {
            marginStyle = MARGIN_LEFT
          } else if (index === 2) {
            marginStyle = MARGIN_RIGHT
          }

          const onPress = () => {
            const event = navigation.emit({
              type: "tabPress",
              target: route.key,
              canPreventDefault: true,
            })

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name)
            }
          }
          return (
            <Button key={route.key} vertical onPress={onPress} style={marginStyle}>
              <IconFont active={isFocused} name={iconName} style={iconStyle} />
            </Button>
          )
        })}
      </FooterTab>
    </Footer>
  )
})
