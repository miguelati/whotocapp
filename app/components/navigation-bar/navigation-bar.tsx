import * as React from "react"
import { TextStyle, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { useNavigation } from "@react-navigation/native"
import { Header, Left, Right, Body, Icon, Button, Title } from "native-base"
import { Text } from "../text/text"
import { PowerButton } from "../power-button/power-button"
import { translate } from "../../i18n/translate"

import { createIconSetFromFontello } from "react-native-vector-icons"
import fontelloConfig from "../../config/icons.json"
const IconFont = createIconSetFromFontello(fontelloConfig)

const TEXT_BUTTON: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.palette.navy,
}

const LEFT_CONTENT: ViewStyle = {
  marginLeft: 30,
}

const RIGHT_CONTENT: ViewStyle = {
  marginRight: 30,
}

const BODY_STYLE: ViewStyle = {
  alignItems: "flex-start",
  marginLeft: -50,
}

const TITLE_STYLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 24,
  color: color.palette.navy,
}

const ICON: TextStyle = {
  fontSize: 24,
  color: color.palette.navy,
}

const CENTER_STYLE: ViewStyle = {
  alignItems: "center",
}

export interface NavigationBarProps {
  /**
   * An optional style override useful for padding & margin.
   */
  title?: string
  canBack?: boolean
  hasDiscovery?: boolean
  center?: boolean
  edit?: () => void
  completeLater?: () => void
}

/**
 * Describe your component here
 */
export const NavigationBar = observer(function NavigationBar(props: NavigationBarProps) {
  const { title, canBack, hasDiscovery, center, edit, completeLater } = props

  const navigation = useNavigation()

  const onPressBackButton = () => {
    navigation.goBack()
  }

  const onPressPowerButton = () => {
    console.log("hasDiscovery")
  }

  const onPressList = () => {
    console.log("List")
  }

  let bodyStyle: ViewStyle | [ViewStyle, ViewStyle] = BODY_STYLE
  if (center) {
    bodyStyle = [bodyStyle, CENTER_STYLE]
  }

  return (
    <Header noShadow transparent>
      <Left style={LEFT_CONTENT}>
        {canBack && (
          <Button transparent onPress={onPressBackButton}>
            <IconFont style={ICON} name="back" />
          </Button>
        )}
        {hasDiscovery && (
          <Button transparent onPress={onPressList}>
            <IconFont style={ICON} name="list" />
          </Button>
        )}
      </Left>
      <Body style={bodyStyle}>
        {title && <Title style={TITLE_STYLE}>{translate(title, { defaultValue: title })}</Title>}
      </Body>
      <Right style={RIGHT_CONTENT}>
        {hasDiscovery && <PowerButton onPress={onPressPowerButton} />}
        {completeLater && (
          <Button transparent onPress={completeLater}>
            <Text style={TEXT_BUTTON} tx="navigationBar.completeLater" />
          </Button>
        )}
        {edit && (
          <Button transparent onPress={edit}>
            <Text style={TEXT_BUTTON} tx="navigationBar.edit" />
          </Button>
        )}
      </Right>
    </Header>
  )
})
