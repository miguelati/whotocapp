import React, { useState } from "react"
import { TextStyle, View, ViewStyle, TouchableOpacity } from "react-native"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { Row, Col, Icon, Input, Item } from "native-base"
import { SelectPhoneCode } from "../select-phone-code/select-phone-code"

const CONTAINER: ViewStyle = {
  flex: 1,
  height: 80,
  paddingLeft: 30,
  paddingRight: 30,
}

const TITLE_CONTAINER: ViewStyle = {
  alignItems: "flex-end",
  marginBottom: 10,
}

const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.palette.navy50,
}

const CODE_PHONE: TextStyle = {
  color: color.textWhite,
  fontFamily: typography.bold,
  fontSize: 14,
  marginRight: 7,
}

const BUTTON_SELECT: ViewStyle = {
  backgroundColor: color.buttonWhiteText,
  borderRadius: 3,
  flexDirection: "row",
  justifyContent: "space-around",
  alignItems: "center",
}

const ICON: TextStyle = {
  color: color.textWhite,
}

const ITEM: ViewStyle = {
  position: "relative",
  top: -12,
}

const INPUT: TextStyle = {
  color: color.textNavy,
  fontFamily: typography.bold,
  fontSize: 14,
}

export interface PhoneInputProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  phone?: string

  phoneCode?: string

  onChangeText?: (code: string, phoneCode: string) => void

  onBlur?: (e: any) => void
}

/**
 * Describe your component here
 */
export function PhoneInput(props: PhoneInputProps) {
  const { style, phone, phoneCode, onChangeText, onBlur } = props

  const [showSelector, setshowSelector] = useState(false)
  // const [phoneCode, setPhoneCode] = useState(phoneCodeValue)
  // const [phone, setPhone] = useState(phoneValue)

  const onShowCodeSelectorPress = () => {
    setshowSelector(true)
  }

  const onCloseCodeSelectorPress = () => {
    setshowSelector(false)
  }

  const onPhoneCodeSelected = (value: string) => {
    onChangeText(phone, value)
  }

  const onChange = (value: string) => {
    onChangeText(value, phoneCode)
  }

  const onBlurInput = (e: any) => {
    onBlur(e)
  }

  return (
    <View style={[CONTAINER, style]}>
      <Row style={TITLE_CONTAINER}>
        <Col>
          <Text style={TITLE} tx="phoneInput.title" />
        </Col>
      </Row>
      <Row size={1}>
        <Col>
          <TouchableOpacity style={BUTTON_SELECT} onPress={onShowCodeSelectorPress}>
            <Icon style={ICON} name="chevron-down" type="Entypo" />
            <Text style={CODE_PHONE}>{phoneCode}</Text>
          </TouchableOpacity>
        </Col>
        <Col size={4}>
          <Item inlineLabel style={ITEM}>
            <Input
              style={INPUT}
              value={phone}
              onChangeText={onChange}
              onBlur={onBlurInput}
              keyboardType="phone-pad"
              placeholder="Phone number"
            />
          </Item>
        </Col>
      </Row>
      <SelectPhoneCode
        visible={showSelector}
        onClose={onCloseCodeSelectorPress}
        onSelected={onPhoneCodeSelected}
      />
    </View>
  )
}
