import * as React from "react"
import {
  View,
  ViewStyle,
  ImageStyle,
  TouchableOpacity,
  StyleProp,
  TextStyle,
  Image,
} from "react-native"
import { Instance } from "mobx-state-tree"
import { DiscoverModel } from "../../models"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { TocButton } from "../toc-button/toc-button"

const horizontalMargin = 20
const slideWidth = 300
const itemWidth = slideWidth + horizontalMargin * 2
const itemHeight = 400

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const SLIDE: ViewStyle = {
  marginTop: 50,
  width: itemWidth,
  height: itemHeight,
  paddingHorizontal: horizontalMargin,
}

const SLIDER_INNER_CONTAINER: ViewStyle = {
  width: slideWidth,
  flex: 1,
}

const IMAGE_CONTAINER: ViewStyle = {
  alignItems: "center",
  justifyContent: "center",
}

const ITEM_BORDER: ViewStyle = {
  width: slideWidth - 10,
  backgroundColor: color.palette.whiteShine,
  borderRadius: (slideWidth - 10) / 2,
  height: slideWidth - 10,
  alignItems: "center",
  justifyContent: "center",
  shadowColor: color.palette.shine,
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity: 0.9,
  shadowRadius: 30,
  elevation: 24,
}

const ITEM_IMAGE: StyleProp<ImageStyle> = {
  width: slideWidth - 40,
  borderRadius: (slideWidth - 40) / 2,
  height: slideWidth - 40,
}

const DETAIL_CONTAINER: ViewStyle = {
  marginTop: 30,
}

const NICK: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 28,
  color: color.palette.navy,
  textAlign: "center",
}

const PROFESSION: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 18,
  color: color.palette.navy,
  textAlign: "center",
}

const COLLAGE: TextStyle = {
  fontFamily: typography.regular,
  fontSize: 18,
  color: color.palette.navy,
  textAlign: "center",
}

const WHOTOC_BUTTON_CONTAINER: ViewStyle = {
  alignItems: "center",
  justifyContent: "center",
  marginTop: 10,
}

export interface RadarCarouselItemProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  user: Instance<typeof DiscoverModel>
  showProfileTap: (user: Instance<typeof DiscoverModel>) => void
}

/**
 * Describe your component here
 */
export function RadarCarouselItem(props: RadarCarouselItemProps) {
  const { style, user, showProfileTap } = props

  const onPress = () => {
    showProfileTap(user)
  }

  return (
    <View style={[CONTAINER, style]}>
      <View style={SLIDE}>
        <View style={SLIDER_INNER_CONTAINER}>
          <TouchableOpacity onPress={onPress}>
            <View style={IMAGE_CONTAINER}>
              <View style={ITEM_BORDER}>
                <Image style={ITEM_IMAGE} source={{ uri: user.profile.picture, cache: "reload" }} />
              </View>
            </View>
            <View style={DETAIL_CONTAINER}>
              <Text style={NICK}>{user.profile.getNameWithAge(false)}</Text>
              <Text style={PROFESSION}>{user.profile.profession}</Text>
              <Text style={COLLAGE}>{user.profile.collage}</Text>
            </View>
          </TouchableOpacity>
          <View style={WHOTOC_BUTTON_CONTAINER}>
            <TocButton />
          </View>
        </View>
      </View>
    </View>
  )
}
