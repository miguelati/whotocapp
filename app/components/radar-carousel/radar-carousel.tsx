import * as React from "react"
import { View, ViewStyle, Dimensions } from "react-native"
import Carousel from "react-native-snap-carousel"
// import { color, typography } from "../../theme"
// import { Text } from "../"
import { Instance } from "mobx-state-tree"
import { DiscoverModel } from "../../models"
import { RadarCarouselItem } from "./radar-carousel-item"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  height: 600,
}

export interface RadarCarouselProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  users: Array<typeof DiscoverModel>
  onWhotocTap: (user: Instance<typeof DiscoverModel>) => void
}

/**
 * Describe your component here
 */
export function RadarCarousel(props: RadarCarouselProps) {
  const { style, users, onWhotocTap } = props

  const horizontalMargin = 20
  const slideWidth = 300
  const sliderWidth = Dimensions.get("window").width
  const itemWidth = slideWidth + horizontalMargin * 2

  const renderItem = ({ index, item }) => {
    return <RadarCarouselItem key={index} user={item} showProfileTap={onWhotocTap} />
  }

  return (
    <View style={[CONTAINER, style]}>
      <Carousel
        data={users}
        renderItem={renderItem}
        sliderWidth={sliderWidth}
        itemWidth={itemWidth}
      />
    </View>
  )
}
