import * as React from "react"
import { TouchableOpacity, TextStyle, ViewStyle, View } from "react-native"
import { Text } from "../text/text"
import { color, spacing } from "../../theme"
import { CheckboxProps } from "./checkbox.props"
import { mergeAll, flatten } from "ramda"
import { Icon } from "native-base"

const ROOT: ViewStyle = {
  flexDirection: "row",
  paddingVertical: spacing[1],
  alignSelf: "flex-start",
  alignItems: "center",
}

const DIMENSIONS = { width: 20, height: 20 }

const OUTLINE: ViewStyle = {
  ...DIMENSIONS,
  marginTop: 2, // finicky and will depend on font/line-height/baseline/weather
  justifyContent: "center",
  alignItems: "center",
  borderWidth: 1,
  borderColor: color.palette.navy50,
  borderRadius: 2,
}

const FILL: ViewStyle = {
  width: DIMENSIONS.width - 1,
  height: DIMENSIONS.height - 1,
  backgroundColor: color.palette.orangeLight,
  borderRadius: 2,
}

const ICON: TextStyle = {
  color: color.palette.white,
  fontSize: 17,
}

const LABEL: TextStyle = {
  paddingLeft: spacing[2],
  color: color.palette.navy,
}

export function Checkbox(props: CheckboxProps) {
  const numberOfLines = props.multiline ? 0 : 1

  const rootStyle = mergeAll(flatten([ROOT, props.style]))
  const outlineStyle = mergeAll(flatten([OUTLINE, props.outlineStyle]))
  const fillStyle = mergeAll(flatten([FILL, props.fillStyle]))

  const onPress = props.onToggle ? () => props.onToggle && props.onToggle(!props.value) : null

  return (
    <TouchableOpacity
      activeOpacity={1}
      disabled={!props.onToggle}
      onPress={onPress}
      style={rootStyle}
    >
      <View style={outlineStyle}>
        {props.value && (
          <View style={fillStyle}>
            <Icon style={ICON} name="check" type="Entypo" />
          </View>
        )}
      </View>
      <Text text={props.text} tx={props.tx} numberOfLines={numberOfLines} style={LABEL} />
    </TouchableOpacity>
  )
}
