import * as React from "react"
import { View, ViewStyle, ImageBackground } from "react-native"
import { observer } from "mobx-react-lite"
import { Instance } from "mobx-state-tree"
import BackgroundGeolocation, { Location } from "react-native-background-geolocation"
import { RadarCarousel } from "../radar-carousel/radar-carousel"
import { DiscoverEmpty } from "../discover-empty/discover-empty"
import { useStores, DiscoverModel, RootStore } from "../../models"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  flex: 1,
}

const BACKGROUND: ViewStyle = {
  width: "100%",
  height: "100%",
}

export interface RadarProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
}

const getDefaultConfig = () => {
  return {
    // Geolocation Config
    desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_HIGH,
    distanceFilter: 30,
    // Activity Recognition
    stopTimeout: 1,
    // Application config
    debug: false, // <-- enable this hear sounds for background-geolocation life-cycle.
    logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
    stopOnTerminate: false, // <-- Allow the background-service to continue tracking when user closes the app.
    startOnBoot: true, // <-- Auto start tracking when device is powered-up.
  }
}

/**
 * Describe your component here
 */
export const Radar = observer(function Radar(props: RadarProps) {
  const { style } = props
  const stores = useStores()

  const sendLocation = (location: Location, stores: RootStore) => {
    const { latitude, longitude } = location.coords
    stores.discoverStore.add(latitude, longitude)
  }

  React.useEffect(() => {
    const options = {
      timeout: 30, // 30 second timeout to fetch location
      maximumAge: 5000, // Accept the last-known-location if not older than 5000 ms.
      desiredAccuracy: 10, // Try to fetch a location with an accuracy of `10` meters.
      samples: 3, // How many location samples to attempt.
    }
    BackgroundGeolocation.getCurrentPosition(options, (location) => sendLocation(location, stores))
    BackgroundGeolocation.onLocation(
      (location) => sendLocation(location, stores),
      (error) => console.log("onError", error),
    )

    BackgroundGeolocation.ready(getDefaultConfig(), (state) => {
      console.log(`- BackgroundGeolocation is configured and ready: ${state.enabled}`)

      if (!state.enabled) {
        BackgroundGeolocation.start(() => console.log("- Start success"))
      }
    })
  }, [])

  const onPressUser = (user: Instance<typeof DiscoverModel>) => {
    console.log("onPressUser ", user)
  }

  const users: Array<typeof DiscoverModel> = stores.discoverStore.inRadar()

  return (
    <View style={[CONTAINER, style]}>
      {users.length === 0 && <DiscoverEmpty />}
      {users.length > 0 && (
        <ImageBackground source={require("./img/bkg.png")} style={BACKGROUND}>
          <RadarCarousel users={users} onWhotocTap={onPressUser} />
        </ImageBackground>
      )}
    </View>
  )
})
