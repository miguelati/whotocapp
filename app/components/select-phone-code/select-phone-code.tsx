import * as React from "react"
import {
  TextStyle,
  View,
  ViewStyle,
  FlatList,
  ListRenderItem,
  TouchableOpacity,
} from "react-native"
import Modal from "react-native-modal"
import { Row, Col, Button } from "native-base"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import codes from "./codes.json"

const CONTAINER: ViewStyle = {
  justifyContent: "flex-end",
  margin: 0,
}

const TEXT_NORMAL: TextStyle = {
  fontFamily: typography.semiBold,
  fontSize: 14,
  color: color.palette.navy,
}

const VIEW: ViewStyle = {
  width: "100%",
  height: 300,
  backgroundColor: "#FFF",
}

const BUTTON_TEXT: TextStyle = {
  fontFamily: typography.semiBold,
  fontSize: 14,
  color: color.palette.white,
}

const TEXT_TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 14,
  color: color.palette.white,
}

const ROW_HEADER: ViewStyle = {
  height: 40,
  backgroundColor: color.palette.navy,
}

const COL_LEFT_CENTER: ViewStyle = {
  alignItems: "flex-start",
  justifyContent: "center",
}

const COL_RIGHT_CENTER: ViewStyle = {
  alignItems: "flex-end",
  justifyContent: "center",
}

const MARGIN_30: ViewStyle = {
  marginLeft: 30,
}

const ROW_ITEM: ViewStyle = {
  paddingLeft: 30,
  paddingRight: 30,
  paddingTop: 16,
  paddingBottom: 16,
  borderBottomColor: color.palette.grey,
  borderBottomWidth: 1,
}

export interface SelectPhoneCodeProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  onSelected?: (value: string) => void

  visible: boolean

  onClose?: () => void
}

export interface PhoneCode {
  name: string
  dialCode: string
  code: string
}

/**
 * Describe your component here
 */
export function SelectPhoneCode(props: SelectPhoneCodeProps) {
  const { visible, onClose, onSelected } = props

  const renderItem: ListRenderItem<PhoneCode> = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          onSelected(item.dialCode)
          onClose()
        }}
      >
        <Row style={ROW_ITEM}>
          <Col>
            <Text style={TEXT_NORMAL} text={`${item.name} (${item.code})`} />
          </Col>
          <Col style={COL_RIGHT_CENTER}>
            <Text style={TEXT_NORMAL} text={item.dialCode} />
          </Col>
        </Row>
      </TouchableOpacity>
    )
  }

  return (
    <Modal isVisible={visible} style={CONTAINER}>
      <View style={VIEW}>
        <Row style={ROW_HEADER}>
          <Col style={[COL_LEFT_CENTER, MARGIN_30]}>
            <Button transparent onPress={onClose}>
              <Text style={BUTTON_TEXT} tx="selectPhoneCode.cancel" />
            </Button>
          </Col>
          <Col size={3} style={COL_LEFT_CENTER}>
            <Text style={TEXT_TITLE} tx="selectPhoneCode.title" />
          </Col>
        </Row>
        <Row>
          <Col>
            <FlatList<PhoneCode>
              data={codes as PhoneCode[]}
              renderItem={renderItem}
              keyExtractor={(item, index) => `${index}`}
            />
          </Col>
        </Row>
      </View>
    </Modal>
  )
}
