import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { color } from "../../theme"
import { SelectPhoneCode } from "./select-phone-code"

storiesOf("SelectPhoneCode", module)
  .addDecorator((fn) => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <SelectPhoneCode visible={false} style={{ backgroundColor: color.error }} />
      </UseCase>
    </Story>
  ))
